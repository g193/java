public class FlourPacker {
    public static boolean canPack(int bigCount, int smallCount, int goal) {
        if (bigCount >= 0 && smallCount >= 0 && goal >= 0) {
            if (goal <= smallCount) return true;
            int fives_in_goal = goal / 5;
            int remainder = goal % 5;
            if (bigCount >= fives_in_goal) {
                if (remainder <= smallCount) return true;
            } else {
                if (goal - bigCount * 5 - smallCount <= 0) return true;
            }
        }
        return false;
    }
}
