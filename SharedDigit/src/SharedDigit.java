public class SharedDigit {
    public static boolean hasSharedDigit(int number_a, int number_b) {
        if (number_a < 10 || number_a > 99 || number_b < 10 || number_b > 99) {
            return false;
        }
        int num_b;
        int num_a = number_a;
        int num = 0;
        while (number_a > 0) {
            num_b = number_b;
            num_a = number_a % 10;
            while (num_b > 0) {
                num = num_b % 10;
                if (num_a == num) return true;
                num_b = num_b / 10;
            }
            number_a = number_a / 10;
        }
        return false;
    }
}
