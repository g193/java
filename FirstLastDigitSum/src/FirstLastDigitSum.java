public class FirstLastDigitSum {
    public static int sumFirstAndLastDigit(int number) {
        if (number < 0) return -1;
        int sum = number % 10; //last digit
        int num = 0;
        while (number > 0) {
            num = number % 10;
            number /= 10;
        }
        sum += num;
        return sum;
    }
}
